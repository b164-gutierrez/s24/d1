				//Miniactivity 03-23-22

				// 2. $or Operator - hide _id field

db.users.find({$or: [{firstName: {$regex: 's', $options: '$i'}}, 
    {lastName: {$regex: 'd', $options: '$i'}}] },
    {_id:0}
)




				// 3. $and, $regex and $lte Operator

db.users.find({ $and: [{ department: 'HR'}, { age: { $gte: 70 } }] 

})




				// 4. Use the $and, $regex and $lte operators

db.users.find({ $and: [{ firstName: {$regex: 'e', $options: '$i'}}, { age: { $lte: 30 } }] 

})
