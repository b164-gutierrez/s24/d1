// Comparison Query Operators

// $gt(greater than), $gte Operator

/*
	syntax:
			db.collectionName.find({field: {$gt : number value}})
			db.collectionName.find({field: {$gte : number value}})
*/
db.users.find({age: {$gt: 76}})
db.users.find({age: {$gte: 76}})


// $lt(less than), $lte(less than or equal) Operator

/*
	syntax:
			db.collectionName.find({field: {$lt : number value}})
			db.collectionName.find({field: {$lte : number value}})
*/
db.users.find({age: {$lt: 76}})
db.users.find({age: {$lte: 76}})

// $ne(not equal)
/*
	syntax:
			db.collectionName.find({field: {$ne : number value}})
*/
			
db.users.find({age: {$ne: 76}})


// $in Operator
// allows us to find documents with specific match criteria one field using different values.
/*
	syntax:
			db.collectionName.find({field: {$in : value}})
*/

// sample
db.users.find({lastName: {$in: ['Hawking', 'Doe']}})
db.users.find({courses: {$in: ['Sass', 'React']}})



					// Logical Query Operator

// $or Operator
// allows us to find documents that match a single criteria from multiple provided search criteria.

/*
	syntax:
			db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}] })
*/

// sample
db.users.find({$or: [{firstName: 'Neil'}, {age: 25}] })

db.users.find({$or: [{firstName: 'Neil'}, {age: {$gt: 30}}]})



// $and Operator
// allows us to find documents matching multiple criteria in a single field.

/*
	syntax:
			db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}] })
*/

// sample
db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 }} ] })





					// Field Projection
// Inclusion
// allows us to include/add specific fields only when retrieving documents.
// the value can be provided to 1 to denote the field is being included.
/*
	syntax:
			db.collectionName.find({criteria})
*/

// sample
db.users.find({firstName: 'Jane'}, 
	{
    firstName:1,
    lastName:1,
    contact:1
    
    })




// Exclusion
// 0 denotes the exception of field
/*
	syntax:
			db.collectionName.find({criteria})
*/


// sample
db.users.find({firstName: 'Jane'}, 
	{
    firstName:1,
    lastName:1,
    contact:1,
    _id: 0
    
    })



// embedded
// sample
db.users.find({firstName: 'Jane'}, {
    firstName:1,
    lastName:1,
    contact:{phone:1},
    _id: 0
    
    })



// dot notation
// sample1
db.users.find({firstName: 'Jane'}, {
    firstName:1,
    lastName:1,
    'contact.phone':1,
    _id: 0
    
    })

// sample2
db.users.find({firstName: 'Jane'}, {
   
    'contact.phone':0,
    
    
    })



// field projection and slice operator
// $slice operator allows us to retrieve portions of element that matches the search criteria.

// sample
db.users.find({}, {
   
    _id: 0,
    courses: {$slice: [1, 2]}
    
    
    })



//Evalution Query Operator
//$regex  Operator (case sensitive)
// allows us to find documents that match

/*
	syntax:
			db.users.find({field: $regex: 'pattern'}) //case sensitive query

			db.users.find({field: $regex: 'pattern', $options: '$i'}) //case insensitive query
*/

// sample1 - case sensitive query
db.users.find({  firstName: { $regex: 'N'}  })//Neil

db.users.find({  firstName: { $regex: 'J'}  })//Jane

db.users.find({  firstName: { $regex: 'n'}  })//all name with (n)


// sample2 - case insensitive query

db.users.find({  firstName: { $regex: 'j', $options: '$i'}  })

//here $options with '$i' value specifies that we want to carry out search without considering the case sensitivity.